import base64

import boto3
from botocore.exceptions import ClientError
import docker
import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger


logger = get_logger()

schema = {
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},
    'IMAGE_NAME': {'type': 'string', 'required': True},
    'TAGS': {'type': 'string', 'required': False, 'nullable': True, 'default': 'latest'},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}

}


class ECRPush(Pipe):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.docker_client = None

    def get_client(self):
        try:
            return boto3.client('ecr', region_name=self.get_variable('AWS_DEFAULT_REGION'))
        except ClientError as err:
            self.fail("Failed to create boto3 client.\n" + str(err))

    def get_docker_client(self):
        if self.docker_client is None:
            self.docker_client = docker.from_env()
        return self.docker_client

    def docker_login(self, docker_client):
        client = self.get_client()
        response = client.get_authorization_token()
        b64token = response['authorizationData'][0]['authorizationToken']
        username, password = base64.b64decode(
            b64token).decode('utf-8').split(':')

        registry = response['authorizationData'][0]['proxyEndpoint']

        try:
            docker_client.login(username=username, password=password, registry=registry)
        except docker.errors.APIError as error:
            self.fail(f"Docker login error: {error}")

        logger.info(f'Successfully logged in to {registry}')

        return registry

    def push(self, docker_client, image_name, tags, serveraddress):

        for tag in tags:
            self.tag_image(image_name, tag, serveraddress)
            self.push_image(image_name, tag, serveraddress)

    def tag_image(self, image_name, tag, serveraddress):
        docker_client = self.get_docker_client()
        try:
            image = docker_client.images.get(image_name)
        except docker.errors.ImageNotFound as error:
            self.fail(f"Image not found: {error}")

        try:
            result = image.tag(f"{serveraddress}/{image_name}", tag=tag)
        except docker.errors.APIError as error:
            self.fail(f"Docker tag error: {error}")
        if not result:
            self.fail(f"Failed to apply tag: {tag}")

    def push_image(self, image_name, tag, serveraddress):
        docker_client = self.get_docker_client()
        for line in docker_client.images.push(f"{serveraddress}/{image_name}",
                                              tag,
                                              stream=True,
                                              decode=True):
            self.log_info(line)
            if line.get('error') is not None:
                self.fail(
                    f"Docker push error: {line['errorDetail']['message']}")

    def run(self):
        super().run()

        self.log_info('Executing the aws-ecr-push-image pipe...')

        image_name = self.get_variable('IMAGE_NAME')

        raw_tags = self.get_variable('TAGS').split()
        tags = [str(tag) for tag in raw_tags]

        docker_client = self.get_docker_client()

        registry = self.docker_login(docker_client)

        serveraddress = registry.split('//')[1]

        self.push(docker_client, image_name, tags, serveraddress)

        console = f"https://{self.get_variable('AWS_DEFAULT_REGION')}.console.aws.amazon.com/ecr/repositories/{image_name}/"
        self.success(
            message=f"Successfully pushed {image_name} to {registry}. \n  The image is available in your ECR repository: {console}")


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/usr/bin/pipe.yml', 'r'))
    pipe = ECRPush(pipe_metadata=metadata, schema=schema, logger=logger, check_for_newer_version=True)
    pipe.run()

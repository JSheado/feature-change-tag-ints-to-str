# Bitbucket Pipelines Pipe: AWS ECR push image

Pushes docker images to the AWS Elastic Container Registry.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-ecr-push-image:1.2.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    IMAGE_NAME: "<string>"
    # TAGS: "<string>" # Optional
    # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**) | AWS access key id |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key |
| AWS_DEFAULT_REGION (**) | AWS region |
| IMAGE_NAME            | The name of the image to push to the ECR. The name should be the same as your ECR repository name. Remember that you don't need to add your registry URL in front of the image name, the pipe will fetch this URL from AWS and add it to the image tag for you.|
| TAGS                  | List of white space separated tags to push. Default: `latest`.|
| DEBUG                 | Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Prerequisites

To use this pipe you should have a IAM user configured with programmatic access, with the necessary permissions to push docker images to your ECR repository. You also need to set up a
ECR container registry if you don't already have on. Here is a [AWS ECR Getting started](https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_GetStarted.html) guide from AWS on how to set up a new registry.

IMPORTANT! This pipe expects the docker image to be built already. You can see the examples below for more details.

## Examples

Useful tip: you can use [pipelines caching](https://confluence.atlassian.com/bitbucket/caching-dependencies-895552876.html) to cache docker layers created in previous builds. See the **Docker layer caching** section in 
[Run Docker commands in Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/run-docker-commands-in-bitbucket-pipelines-879254331.html)

### Basic example:

Building and pushing the image with default options:

```yaml
script:
  # build the image
  - docker build -t my-docker-image .

  # use the pipe to push the image to AWS ECR
  - pipe: atlassian/aws-ecr-push-image:1.2.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      IMAGE_NAME: my-docker-image
```


Example building and pushing the image with default options. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  # build the image
  - docker build -t my-docker-image .

  # use the pipe to push the image to AWS ECR
  - pipe: atlassian/aws-ecr-push-image:1.2.1
    variables:
      IMAGE_NAME: my-docker-image
```

### Advanced example:

Pushing multiple tags: `${BITBUCKET_TAG}` and `latest`:
to tag the image:

```yaml
script:

  # build the image
  - docker build -t my-docker-image .

  # use the pipe to push to AWS ECR
  - pipe: atlassian/aws-ecr-push-image:1.2.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      IMAGE_NAME: my-docker-image
      TAGS: '${BITBUCKET_TAG} latest'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,ecr
